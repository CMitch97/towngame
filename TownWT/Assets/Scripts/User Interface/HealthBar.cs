﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthBar : MonoBehaviour
{
    private Transform bar;

    void Start()
    {
        bar = transform.Find("Bar");
    }

    void Update()
    {

    }

    public void SetSize(float currentHealth)
    {
        bar.localScale = new Vector2(currentHealth * 0.01f, 1f);
    }
}
