﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TimerController : MonoBehaviour
{
    public float currentTime;
    public bool timerActive;
    public Text timerText;

    // Delay for resetting scene
    public float resetDelay;
    bool delayActive;

    void Start()
    {
        timerActive = true; // Activate timer at start of game

        delayActive = false; // Turns off the delay of the level reset until needed
    }

    void Update()
    {
        if (timerActive == true) // If the timer is active, start counting down
        {
            if (currentTime > 0)
            {
                currentTime -= Time.deltaTime;
                DisplayTime(currentTime);

            }
            else
            {
                currentTime = 0; // Stop when timer reaches 0
                timerActive = false;
                timerText.text = "TEMPORAL ERROR:";
                delayActive = true;
            }
        }

        // Restart Level
        if (delayActive == true)
        {
            resetDelay -= Time.deltaTime; // Starts the countdown for resetting the level

            if (resetDelay < 0)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }

    }

    void DisplayTime(float timeToDisplay)
    {
        float minutes = Mathf.FloorToInt(timeToDisplay / 60); // Show time in minutes
        float seconds = Mathf.FloorToInt(timeToDisplay % 60); // Show time in seconds

        timerText.text = "Time Left: " + string.Format("{0:00}:{1:00}", minutes, seconds); // Display time in-game
    }
}
