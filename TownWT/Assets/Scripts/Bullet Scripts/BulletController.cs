﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float bulletSpeed = 20f;
    public Rigidbody2D rb;

    void Start()
    {
        rb.velocity = transform.right * bulletSpeed;
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        // Destroy the bullet objects if they collide with certain objects
        if (col.gameObject.CompareTag("Enemy"))
        {
            Destroy(this.gameObject);
        }

        if (col.gameObject.CompareTag("Ground"))
        {
            Destroy(this.gameObject);
        }
    }
}