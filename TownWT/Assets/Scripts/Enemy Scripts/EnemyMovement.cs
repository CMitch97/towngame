﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    public float speed;
    private float reverseSpeed;
    private bool movingRight;

    // Ground Detection
    public Transform findGround;
    public float rayDistance;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        movingRight = true;

        // Sets the reverse speeds variable to double of the speed variable, this allows it to scale with whatever value I input in the editor
        reverseSpeed = speed * 2;
    }

    void Update()
    {
        rb.velocity = new Vector2(speed, rb.velocity.y);

        // The empty object will check whether there is ground underneath the enemy or not
        RaycastHit2D groundInfo = Physics2D.Raycast(findGround.position, Vector2.down, rayDistance);

        // Flips the enemy depending on what direction they are moving in when they come to the end of the platform
        if (groundInfo.collider == false)
        {
            if (movingRight == true)
            {
                // Removes the reverse value of speed so the enemy changes direction and maintains the same speed
                speed -= reverseSpeed;
                movingRight = false;
                transform.Rotate(0f, 180f, 0f);

            }
            else
            {
                // Adds the reverse value of speed so the enemy changes direction and maintains the same speed
                speed += reverseSpeed;
                movingRight = true;
                transform.Rotate(0f, 180f, 0f);
            }
        }
    }

    // Colliding with walls
    private void OnTriggerEnter2D(Collider2D collision)
    {
        // If the enemies child object collides with the ground, it should change direction
        if (collision.gameObject.CompareTag("Ground"))
        {
            if (movingRight == true)
            {
                // Removes the reverse value of speed so the enemy changes direction and maintains the same speed
                speed -= reverseSpeed;
                movingRight = false;
                transform.Rotate(0f, 180f, 0f);

            }
            else
            {
                // Adds the reverse value of speed so the enemy changes direction and maintains the same speed
                speed += reverseSpeed;
                movingRight = true;
                transform.Rotate(0f, 180f, 0f);
            }
        }

        // If the enemies child object collides with the wall, it should change direction
        if (collision.gameObject.CompareTag("Walls"))
        {
            if (movingRight == true)
            {
                // Removes the reverse value of speed so the enemy changes direction and maintains the same speed
                speed -= reverseSpeed;
                movingRight = false;
                transform.Rotate(0f, 180f, 0f);

            }
            else
            {
                // Adds the reverse value of speed so the enemy changes direction and maintains the same speed
                speed += reverseSpeed;
                movingRight = true;
                transform.Rotate(0f, 180f, 0f);
            }
        }
    }
}
