﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDamage : MonoBehaviour
{
    // Dealing Damage
    public float damageDealt;
    public float attackDelay;
    private float lastAttack;

    // Passing Variables
    public PlayerHealth playerHealth;
    public PlayerCurrency playerCurrency;

    // Currency
    public float moneyToDrop;

    // Health and Damage Taken
    public float enemyMaxHealth;
    private float enemyCurrentHealth;

    public float damageByPistol;
    public float damageBySprayer;

    void Start()
    {
        enemyCurrentHealth = enemyMaxHealth;

        lastAttack = attackDelay;
    }

    void Update()
    {
        // Kill Enemy
        if (enemyCurrentHealth <= 0)
        {
            Destroy(gameObject);
            // Sends the value of moneyToDrop to the player currency script when the enemy dies
            playerCurrency.AddMoney(moneyToDrop);
        }
    }

    // Check to see if the child object has collided with the player object
    public void OnTriggerStay2D(Collider2D playerCollide)
    {
        if (playerCollide.gameObject.tag == "Player")
        {
            Attack();
        }
    }

    // Sends the player script the damage amount and adds a delay for balancing reasons
    public void Attack()
    {
        if (Time.time > lastAttack)
        {
            lastAttack = Time.time + attackDelay;
            playerHealth.TakeDamage(damageDealt);
        }
    }

    // Check to see what kind of bullet has collided with enemy
    public void OnTriggerEnter2D(Collider2D bulletHit)
    {
        if (bulletHit.gameObject.tag == "PistolBullet")
        {
            enemyCurrentHealth -= damageByPistol;
        }
        else if (bulletHit.gameObject.tag == "SprayerBullet")
        {
            enemyCurrentHealth -= damageBySprayer;
        }
    }
}
