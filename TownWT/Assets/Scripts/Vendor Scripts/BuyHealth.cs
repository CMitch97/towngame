﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuyHealth : MonoBehaviour
{
    public float healthCost;
    public float healthAmount;

    private float playerCash;
    private float playerCurrentHealth;

    public PlayerHealth playerHealth;
    public PlayerCurrency playerCurrency;

    // Tests the amount of times health has been bought
    private float totalUsed;

    // Player enters shops collider area
    // Player presses button to buy health
    // Check to see if the player has enough money
    // If they do, add the health and subtract the money
    // If not, do nothing

    public void OnTriggerStay2D(Collider2D shopCollision)
    {
        if (shopCollision.gameObject.CompareTag("Player"))
        {
            if (Input.GetButtonDown("Interact") && playerCash >= healthCost && playerCurrentHealth < 100)
            {
                // Sends the amount of health the vendor will add to the player health script
                playerHealth.AddHealth(healthAmount);
                playerCurrency.RemoveMoney(healthCost);
                totalUsed += 1;
            }
        }
    }

    public void SetCash(float checkCash)
    {
        // Gets the current amount of cash the player has and stores it in a local variable
        playerCash = checkCash;
    }

    public void SetHealth(float checkHealth)
    {
        // Gets the current amount of health the player has and stores it in a local variable
        playerCurrentHealth = checkHealth;
    }
}
