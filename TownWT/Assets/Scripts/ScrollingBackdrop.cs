﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScrollingBackdrop : MonoBehaviour
{
    private float length, startPos;
    public GameObject gameCamera;
    public float scrollEffect;

    void Start()
    {
        startPos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void Update()
    {
        float temp = (gameCamera.transform.position.x * (1 - scrollEffect));
        float dist = (gameCamera.transform.position.x * scrollEffect);

        transform.position = new Vector3(startPos + dist, transform.position.y, transform.position.z);

        if (temp > startPos + length)
        {
            startPos += length;
        }
        else if (temp < startPos - length)
        {
            startPos -= length;
        }
    }
}
