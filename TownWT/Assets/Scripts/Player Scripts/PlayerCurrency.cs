﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerCurrency : MonoBehaviour
{
    public float startMoney;
    private float currentMoney;
    private float moneyToAdd;
    private float moneyToRemove;

    public BuyHealth buyHealth;

    // UI
    public Text moneyText;

    void Start()
    {
        currentMoney = startMoney;
    }

    void Update()
    {
        // UI
        moneyText.text = "Credits: " + currentMoney;
    }

    public void AddMoney(float moneyToDrop)
    {
        // Stores the amount of money dropped by an enemy to a local variable
        moneyToAdd = moneyToDrop;
        currentMoney += moneyToAdd;
    }

    public void RemoveMoney(float moneySpent)
    {
        // Takes the value of the spent money and sets it to a local variable
        moneyToRemove = moneySpent;
        currentMoney -= moneyToRemove;
    }

    public void OnTriggerStay2D(Collider2D shopEnter)
    {
        // If the player enters the shop area, the amount of money they have will be sent to the shop script
        if (shopEnter.gameObject.CompareTag("Vendor"))
        {
            buyHealth.SetCash(currentMoney);
        }
    }
}
