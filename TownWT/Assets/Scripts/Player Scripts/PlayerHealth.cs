﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerHealth : MonoBehaviour
{
    public float maxHealth;
    private float currentHealth;

    public HealthBar healthBar;
    public BuyHealth buyHealth;

    private float damageToTake;
    private float healthToAdd;

    void Start()
    {
        currentHealth = maxHealth;
    }

    void Update()
    {
        // If the health of the player goes below 0, set it back to 0 and kill the player
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            Destroy(gameObject);

            // Reset the level when the player dies
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        // If the player health goes over 100, set it to 100 manually
        if (currentHealth > maxHealth)
        {
            currentHealth = maxHealth;
        }

        // Pass the value of currentHealth through to the health bar script
        healthBar.SetSize(currentHealth);
    }

    public void TakeDamage(float damageDealt)
    {
        // Takes the value of the enemy's damage amount and sets it to a local variable
        damageToTake = damageDealt;
        currentHealth -= damageToTake;
    }
    public void OnTriggerStay2D(Collider2D shopEnter)
    {
        // If the player enters the shop area, the amount of health they have will be sent to the shop script
        if (shopEnter.gameObject.CompareTag("Vendor"))
        {
            buyHealth.SetHealth(currentHealth);
        }
    }

    public void AddHealth(float healPlayer)
    {
        healthToAdd = healPlayer;
        currentHealth += healthToAdd;
    }
}
