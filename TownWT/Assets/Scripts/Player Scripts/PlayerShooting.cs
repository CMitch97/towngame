﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerShooting : MonoBehaviour
{
    public Transform firePoint;
    public GameObject pistolBulletPrefab;
    public GameObject smgBulletPrefab;

    // UI Control
    public Text ammoText;

    // Weapon Switching
    private int weaponChange;

    // Pistol
    public bool pistolActive;
    public int pistolMaxAmmo;
    private int pistolAmmo;
    public float pistolFireRate;
    private float pistolNextFire;
    public float pistolReloadTime;
    private float pistolReloadStart;

    // Sprayer SMG
    public bool sprayerActive;
    public int sprayerMaxAmmo;
    private int sprayerAmmo;
    public float sprayerFireRate;
    private float sprayerNextFire;
    public float sprayerReloadTime;
    private float sprayerReloadStart;

    // Sounds
    private AudioSource gunSoundSource;

    void Start()
    {
        // Get audio component
        gunSoundSource = GetComponent<AudioSource>();

        // Set the starting weapon to the pistol
        weaponChange = 1;

        // Pistol
        pistolActive = true;
        pistolAmmo = pistolMaxAmmo;
        pistolReloadStart = pistolReloadTime;

        // Sprayer SMG
        sprayerActive = false;
        sprayerAmmo = sprayerMaxAmmo;
        sprayerReloadStart = sprayerReloadTime;
    }

    void Update()
    {
        // Switch Weapons
        // On pressing the switch weapon button, the weapon should switch to whatever is next in the list
        if (Input.GetButtonDown("SwitchWeapon"))
        {
            WeaponSwap();
        }

        //  Tap to Fire Weapons
        if (Input.GetButtonDown("Fire1"))
        {
            if (pistolActive == true) // Checks to see if the pistol is the current weapon equipped
            {
                ShootPistol();
            }
        }

        // Hold to Fire Weapons
        if (Input.GetButton("Fire1"))
        {
            if (sprayerActive == true)
            {
                ShootSprayer(); // Checks to see if the SMG is the current weapon equipped
            }
        }

        // Reload Weapons
        ReloadPistol();
        ReloadSprayer();

        // USER INTERFACE
        AmmoCounter();
    }

    public void WeaponSwap()
    {
        // Changes the booleans of the weapons to true or false depending on what the number currently is
        if (weaponChange == 1)
        {
            pistolActive = false;
            weaponChange = 2;
            sprayerActive = true;
        }
        else if (weaponChange == 2)
        {
            pistolActive = true;
            weaponChange = 1;
            sprayerActive = false;
        }
    }
    
    // PISTOL
    private void ShootPistol()
    {
        if (pistolAmmo >= 1)
        {
            // Sets a fire rate for the pistol so it can't be shot too fast
            if (Time.time > pistolNextFire)
            {
                pistolNextFire = Time.time + pistolFireRate; // Sets the pistols fire rate to variable set in editor

                Instantiate(pistolBulletPrefab, firePoint.position, firePoint.rotation); // Spawns bullets at fire point

                pistolAmmo--; // Removes 1 Bullet from pistols magazine

                gunSoundSource.Play(); // Plays audio from audio source

            }
        }
    }

    private void ReloadPistol()
    {
        if (pistolAmmo <= 0)
        {
            // Starts counting down on the time it takes to reload
            pistolReloadStart -= Time.deltaTime;

            // if the timer is up, reset the players ammo count
            if (pistolReloadStart <= 0)
            {
                pistolAmmo = pistolMaxAmmo;
                pistolReloadStart = pistolReloadTime;
            }
        }
    }

    // SPRAYER
    private void ShootSprayer()
    {
        if (sprayerAmmo >= 1)
        {
            // Sets a fire rate for the SMG so it can't be shot too fast
            if (Time.time > sprayerNextFire)
            {
                sprayerNextFire = Time.time + sprayerFireRate; // Sets the SMGs fire rate to variable set in editor

                Instantiate(smgBulletPrefab, firePoint.position, firePoint.rotation); // Spawns bullets at fire point

                sprayerAmmo--; // Removes 1 Bullet from SMG magazine

                // Plays gun sound from audio source
                gunSoundSource.Play();
            }
        }
    }

    private void ReloadSprayer()
    {
        if (sprayerAmmo <= 0)
        {
            // Starts counting down on the time it takes to reload
            sprayerReloadStart -= Time.deltaTime;

            // if the timer is up, reset the players ammo count
            if (sprayerReloadStart <= 0)
            {
                sprayerAmmo = sprayerMaxAmmo;
                sprayerReloadStart = sprayerReloadTime;
            }
        }
    }

    // USER INTERFACE
    private void AmmoCounter()
    {
        // Set ammo text to current weapons ammo count
        if (weaponChange == 1)
        {
            // Display ammo count on screen
            ammoText.text = "Pistol: " + pistolAmmo + "/" + pistolMaxAmmo;
        }
        else if (weaponChange == 2)
        {
            // Display ammo count on screen
            ammoText.text = "Sprayer: " + sprayerAmmo + "/" + sprayerMaxAmmo;
        }
    }




}
