﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUnlocks : MonoBehaviour
{

    // Double Jump
    public bool dubJumpUnlocked;
    private bool jumpPowerUp = false;
    private int jumpPowerUpNum;

    private void Start()
    {
        PlayerPrefs.GetInt("doubleJump");
        jumpPowerUpNum += 1;
}

    private void Update()
    {
        // Double Jump
        // Check the value of the player pref to see if the player has unlocked the double jump upgrade
        if (PlayerPrefs.GetInt("doubleJump") == jumpPowerUpNum)
        {
            jumpPowerUp = true;
            dubJumpUnlocked = jumpPowerUp;
        }
        else
        {
            jumpPowerUp = false;
            dubJumpUnlocked = jumpPowerUp;
        }
        //dubJumpUnlocked = jumpPowerUp;



        // Remove the players double jump / this is for testing purposes only
        if (Input.GetKeyDown(KeyCode.Numlock))
        {
            PlayerPrefs.SetInt("doubleJump", 0);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        // If the player Collides with the collectable, they unlock the double jump
        if (collision.gameObject.CompareTag("Powerup"))
        {
            PlayerPrefs.SetInt("doubleJump", jumpPowerUpNum);
            Destroy(collision.gameObject);
        }
    }
    public void DoubleJump()
    {
        dubJumpUnlocked = jumpPowerUp;
    }
}
