﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    // Movement and Jumping
    private Rigidbody2D rb;
    public float speed;
    public float jumpForce;
    private float moveInput;

    // Collision for jumping
    private bool isGrounded;
    public Transform feetPos;
    public float checkRadius;
    public LayerMask groundCheck;

    // High Jumping Permissions
    private float jumpTimeCounter;
    public float jumpTime;
    bool isJumping;

    // Double Jumping Permissions
    public float doubleJumpForce;
    public float airJumpsAllowed;
    private float airJumpsLeft;
    bool midJump;

    // Movement Unlocks (from another script)
    public PlayerUnlocks playerUnlocks;

    // Flipping Direction
    private bool facingRight;

    // Animations
    private Animator playerAnim;

    void Start()
    {
        // Grabs the animator component from the player
        playerAnim = GetComponent<Animator>();

        // Sets the rb variable to the player objects rigidBody2D component
        rb = GetComponent<Rigidbody2D>();

        // Sets the number of jumps left to the value in the editor
        airJumpsLeft = airJumpsAllowed;

        facingRight = true;
    }

    void FixedUpdate()
    {
        // Finds out if the player presses either move button then moves them in the direction they have pressed
        moveInput = Input.GetAxisRaw("Horizontal");
        rb.velocity = new Vector2(moveInput * speed, rb.velocity.y);

        FlipPlayer(moveInput);
    }

    void Update()
    {
        // Handling animations
        if (moveInput == 0)
        {
            playerAnim.SetBool("isWalking", false);
            playerAnim.SetBool("isIdle", true);
        }
        else
        {
            playerAnim.SetBool("isWalking", true);
            playerAnim.SetBool("isIdle", false);
        }

        // BASIC JUMP AND HOLDING TO JUMP HIGHER
        // Checks to see if a circle below the players feet is colliding with the ground
        isGrounded = Physics2D.OverlapCircle(feetPos.position, checkRadius, groundCheck);

        // Jumps if player is colliding with ground
        if (isGrounded == true && Input.GetButtonDown("Jump"))
        {
            isJumping = true;
            jumpTimeCounter = jumpTime; // Reset the counter to the jump time variable which is set in the editor
            rb.velocity = Vector2.up * jumpForce;
        }

        // Allow the player to jump higher the longer they hold the jump button
        if (Input.GetButton("Jump") && isJumping == true)
        {
            if (jumpTimeCounter > 0)
            {
                rb.velocity = Vector2.up * jumpForce; // Jump if the counter is greater than 0
                jumpTimeCounter -= Time.deltaTime;
            }
            else
            {
                isJumping = false; // If counter is less than 0 then the player cannot jump
            }
        }
        if (Input.GetButtonUp("Jump"))
        {
            isJumping = false; // Don't jump when the player isn't holding the jump button
        }

        // DOUBLE JUMP
        if (playerUnlocks.dubJumpUnlocked == true) // Get the unlock status from another script
        {
            DoubleJump();
        }
    }
    void DoubleJump()
    {
        // This runs if the player has unlocked double jump
        if (isGrounded == true)
        {
            midJump = false;
            airJumpsLeft = airJumpsAllowed; // If touching ground, jumps left is set to the value that is set in the editor

        }
        else if (isGrounded == false)
        {
            midJump = true; // If in air, player is allowed to jump again based on the number set in the editor
        }

        if (Input.GetButtonDown("Jump") && midJump == true && airJumpsLeft > 0)
        {
            rb.velocity = Vector2.up * doubleJumpForce;
            airJumpsLeft--;
        }
    }

    private void FlipPlayer (float moveInput)
    {
        if (moveInput > 0 && !facingRight)
        {
            // Change the direction that the player is facing
            facingRight = !facingRight;

            // Rotate the player sprite if we are facing left instead of right
            transform.Rotate(0f, 180f, 0f);
        }
        else if (moveInput < 0 && facingRight)
        {
            // Change the direction that the player is facing
            facingRight = !facingRight;

            // Rotate the player sprite if we are facing right instead of left
            transform.Rotate(0f, 180f, 0f);
        }
    }
}
